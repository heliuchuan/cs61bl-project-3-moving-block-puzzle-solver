import java.util.*;
/////////////////////////////////////////////////////////////////////////////////////////////
//A Block in an object that has position attributes.									    /
//The move method allows these positions to be changed according to certain moves.          /
//Other methods output information about the state of the Block, such as its neighboring    /
//points and area. A blocks position will always be within the boundaries of the Board      /
//it is located in.																		    /
/////////////////////////////////////////////////////////////////////////////////////////////
public class Block {
	//Top left row
	private int rowStart;
	//Top left column
	private int colStart;
	//Bottom left row
	private int rowEnd;   
	//Bottom left column
	private int colEnd;
	
	//The Block constructor. Takes in a String containing the four ints that define the Block.
	//Scans the String to set the Blocks instance variables.
	public Block(String bounds){
		Scanner s = new Scanner(bounds);
		rowStart = s.nextInt();
		colStart = s.nextInt();
		rowEnd = s.nextInt();
		colEnd = s.nextInt();
	}
	
	//Encapsulation getter methods which return dimensional variables of Block object.	
	 public int getRowStart(){
		 return this.rowStart;
	 }
	 
	 public int getRowEnd(){
		 return this.rowEnd;
	 }
	 
	 public int getColStart(){
		 return this.colStart;
	 }
	 
	 public int getColEnd(){
		 return this.colEnd;
	 }
	 ////////////////////////////////////////////////////////////////////////////////////
	 
	//Method returns a String representation of this Block. The String consists of the 
	//four boundary points of the Block. 
	public String toString() {
		return ""+rowStart+" "+colStart+" "+rowEnd+" "+colEnd;
	}
	
	//Returns the area covered by the block. The area is calculated
	//using the boundary points.
	public double area() {
		double length = colEnd - colStart + 1;
		double height = rowEnd - rowStart + 1;
		return length*height;
	}
	
	//Changes the instance variable of the Block to reflect a move
	//of the Block's position. It is assumed that the move is possible;
	//ie: it does not violate any of the rules of the Board which contains
	//this Block.
	public void move(String direction){ 
		if(direction.equals("up")){
			rowStart--;  
			rowEnd--;
		}
		if(direction.equals("down")){
			rowStart++;
			rowEnd++;
		}
		if(direction.equals("left")){
			colStart--;
			colEnd--;
		}
		if(direction.equals("right")){
			colStart++;
			colEnd++;
		}
	}
	
	//A method that returns a list of Point objects
	//that includes every point contained within the Block.
	public ArrayList<Point> contains() {
		ArrayList<Point> contains = new ArrayList<Point>();
		for(int i = rowStart; i <= rowEnd; i++){
			for(int j = colStart; j <= colEnd; j++){
				contains.add(new Point(i, j));
			}
		}
		return contains;
		
	}
	
	//Checks whether two blocks are equal by comparing their 
	//String representations. Returns true or false.
	public boolean equals(Object b){
		return this.toString().equals(((Block)b).toString());
	}
	
	

}
