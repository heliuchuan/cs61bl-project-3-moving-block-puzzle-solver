import java.util.*;
//Project 3, CS61BL
//Mohini Bariya, Oliver He, and Yeung John Li

/////////////////////////////////////////////////////////////////////////////////////////
//A Board holds Block objects and has a specific configuration at any time. The Board   /
//has a goal configuration which it attempts to reach by moving its Blocks. The Board   /
//stores information about its state, such as empty regions and block location. The     /
//underlying structure is a 2D Array.                                                   /
/////////////////////////////////////////////////////////////////////////////////////////
public class Board {
	private int rows;  
	private int cols;  
	//Goal board configuration (stored as toString() of goal board)
	private String goal;
	//Direction of last move made. Can be "forward" or "reverse"
	private String lstDirection;
	//Active debugging state.
	private ArrayList<String> debugging; 
	
	//A list of Block objects on this Board.
	private ArrayList<Block> blockList; 
	//2D Array representation. Holds block references @ block locations and null
	//@ empty places.
	private Block[][] tray;
	//Empty object that stores info about empty regions of Board: -which squares
	//empty -which Blocks border empty squares.
	private Empty empty;
	//A HashSet containing String representations of all Boards encountered during
	//configuration search.
	private HashSet<String> pastBoards;
	//A hashcount size counter that is used in debugging output.
	private int hashSize;
	//A stack that holds possible moves. Moves are pushed & popped from the stack
	// and implemented in order to reach goal configuration.
	private Stack<String> Moves;
	
	
	//Board constructor. Arguments: rows & 
	//columns.
	public Board(int rows, int cols){
		//A Board with r rows has rows from 0 to r-1.
		this.rows = rows - 1;
		//A Board with c columns has columns from 0 to c-1.
		this.cols = cols - 1 ;
		lstDirection = "forward";
		goal = "";  //Default goal is empty.
		debugging = new ArrayList<String>(); ///Default debugging output is none.
		
		blockList = new ArrayList<Block>();
		tray = new Block[rows][cols];
		empty = new Empty();
		pastBoards = new HashSet<String>();
		Moves = new Stack<String>(); 
	}
	
	
	//////////////////////////////////////////////////////////////////////////////
	//Board initialization methods. These set up Board state in preparation for /
	//trying to achieve the goal configuration.								    /
	/////////////////////////////////////////////////////////////////////////////
	
	//Method adds Block objects to the Board. It changes the tray accordingly
	//and updates the blockList. After call, tray will contain Block reference
	//or null according to configuration.
	public void addBlock(Block b){
		//Add block to the board
		blockList.add(b);
		ArrayList<Point> pointsToAdd = b.contains();
		for(int i = 0; i < pointsToAdd.size(); i++){
			Point curr = pointsToAdd.get(i);
			tray[curr.getRow()][curr.getCol()] = b;
		}
	}
	
	//A dummy method for encapsulation. Called after all Blocks have been added to the Board.
	//Updates the empty Object of this Board by calling the updateisEmpty() method in the Empty
	//class.
	public void EmptyUpdate() {
		this.empty.updateisEmpty();
	}
	
	
	//Method sets Board b as goal for this Board. The toString() of
	//b is saved under goal.
	public void setGoal(Board b){
		this.goal = b.toString();
	}
	
	//This method sets the debugging output for this Board.
	//The debugging output is entered as a command line 
	//argument by the user.
	public void setDebuggingOutput(ArrayList<String> options) throws Exception{
		for(int i = 0; i < options.size(); i++){
			String currOpt = options.get(i);
			if(currOpt.equals("-otime")){
				options.add("time"); //Prints out runtime of solver.
			}
			if(currOpt.equals("-oboards")){
				options.add("boards"); //Prints out Strings of board as moves made.
			}
			if(currOpt.equals("-omoves")){
				options.add("moves"); //Prints out moves made.
			}
			if(currOpt.equals("-ohashsize")){
				options.add("hashsize"); //Prints out size of hashtable containing past Boards.
				this.hashSize = 0;
			}
			if(currOpt.equals("-oempty")){ //Prints out information about empty object.
				options.add("empty");
			}
			if(currOpt.equals("-ovisual")){
				options.add("visual"); //Prints out visual of Board after every move.
			}else {
				throw new IllegalArgumentException("Bad debugging input.");
			}
			
		}
		
	}
	
	//Calculates the density of an initialized Board (all Blocks added)
	//by dividing area occupied by Blocks by total Board area. Used to 
	//determine Solving method.
	private double density(){
		double BlockArea = 0;
		double BoardArea = (rows+1) * (cols+1);
		for(int b = 0; b < this.blockList.size(); b++){
			BlockArea = BlockArea + blockList.get(b).area();
		}
		return BlockArea/BoardArea;
	}
	/////////////////////////////////////////////////////////////////////////////////////
	
	
	////////////////////////////////////////////////////////////////////////////////////
	//A loop that tries to make the Board reach goal configuration. Method makes moves /
	//from Moves stack. After each move, re-populates Stack with next set of possible  /
	//moves. Loop continues while Stack is not empty.                                  /
	////////////////////////////////////////////////////////////////////////////////////
	public String Solve(){
		
		///////////Debugging output//////////////////////////
		Timer t = new Timer();
		if(this.debugging.contains("time")){
			t.start();
		}
		////////////////////////////////////////////////////
		
		//Check for puzzle that is already solved. 
		if(this.isComplete()){
			return "solved";  
		}
		
		nextMoves();
		
		//Start of solving loop.
		while(!this.Moves.isEmpty()){
			String move = Moves.pop();
			makeMove(move);
			
			/////////Debugging output /////////////
			if(this.debugging.contains("boards")){
				System.out.println(this.toString());
			}
			
			if(this.debugging.contains("empty")){
				System.out.println("These points are empty:");
				for(int i = 0; i < this.empty.isEmpty.size(); i++){
					System.out.println(this.empty.isEmpty.get(i) + "/");
				}
				System.out.println("-----------------------------------");
				System.out.println("Blocks next to empty:");
				for(int i = 0; i < this.empty.nextToEmpty.size(); i++){
					System.out.println(this.empty.nextToEmpty.get(i) + "/");
				}
			}
			////////////////////////////////////////
			
			//Check if goal configuration achieved.
			if(this.isComplete()){
				////Debugging output///////////////
				if(this.debugging.contains("time")){
					long time = t.stop()/1000;
					System.out.println("Runtime: " + time + " seconds.");
				}
				//////////////////////////////////
				return "solved";
			}
			//Check if current Board has been previously encountered.
			if(pastBoards.contains(this.toString()) && this.lstDirection.equals("forward")){
				String rev = Moves.pop();
				makeMove(rev);
			}
			//If last move was a "forward" move, add Board configuration to pastBoards
			//hashSet and re-populate stack of Moves.
			if(this.lstDirection.equals("forward")){
				//Debugging output.
				if(this.debugging.contains("hashsize")){
					this.hashSize++;
				}
				pastBoards.add(this.toString());
				nextMoves();
			}
		}
		//If stack is empty and no solution found, return.
		return "No solution.";
	}
	//////////////////////////////////////////////////
	
	//////////////////////////////////////////////////////////////////////////////////////////
	///Method adds all possible next moves onto Moves stack. Moves are found by testing which/
	//direction Blocks next to empty spaces can move. A move that is the reverse of the last /
	//move is not added. For high-density Boards, moves are shuffled and added to stack.	 /
	//////////////////////////////////////////////////////////////////////////////////////////
	private void nextMoves() {
		ArrayList<String> moves = new ArrayList<String>(); 
		ArrayList<Block> track = new ArrayList<Block>();
		ArrayList<Block> Allmovable = this.empty.nextToEmpty;
		for(int i = 0; i < Allmovable.size(); i++){
			Block curr = Allmovable.get(i);
			if(track.contains(curr)){
				
			}else{
				track.add(curr);
				String lstMove = "";
				if(!this.Moves.isEmpty()){
					lstMove = this.Moves.peek();
				}
				
				if(isPossible(curr, "up")){
					String move = SaveMove(curr, "up", "forward");
					if(!sameMove(lstMove, move)){ //Check that this move is not the reverse of the last.
						moves.add(move);
					}
				}
				if(isPossible(curr, "down")){
					String move = SaveMove(curr, "down", "forward");
					if(!sameMove(lstMove, move)){ //Check that this move is not the reverse of the last.
						moves.add(move);
					}
				}
				if(isPossible(curr, "left")){
					String move = SaveMove(curr, "left", "forward");
					if(!sameMove(lstMove, move)){ //Check that this move is not the reverse of the last.
						moves.add(move);
					}
				}
				if(isPossible(curr, "right")){
					String move = SaveMove(curr, "right", "forward");
					if(!sameMove(lstMove, move)){ //Check that this move is not the reverse of the last.
						moves.add(move);
					}
				}
			}
		}
		//Shuffle moves into random order before putting them onto Stack.
		if(this.density() > 0.02){
			Collections.shuffle(moves);
		}
		//Moves in list of possible moves added to Moves stack.
		for(int i = 0; i < moves.size(); i++){
			this.Moves.push(moves.get(i));
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////
	//Method to check if two moves are the same, but disregards whether one is    /
	//"forward" and the other "reverse". Only looks at Block equality and move    /
	//equality. Moves are represented by Strings.								  /
	///////////////////////////////////////////////////////////////////////////////
	private boolean sameMove(String s1, String s2){
		Scanner scan1 = new Scanner(s1);
		Scanner scan2 = new Scanner(s2);
		for(int i = 0; i < 5; i++){ //Elements 0 to 5 in String include Block coordinates & move.
			if(!scan1.hasNext() || !scan2.hasNext()){
				return false;
			}
			if(!scan1.next().equals(scan2.next())){
				return false;
			}
		}
		return true;
	}
	
	
	//Makes a move based on a String representation of a move.
	//String includes Block coordinates, move, and "forward"/"reverse"
	//direction.Uses tray to get reference to Block object to update.
	private void makeMove(String movement){
		
		///////////////////////////////////////////////////
		//Debugging states.								//
		/////////////////////////////////////////////////
		
		if (this.debugging.contains("moves")){
			System.out.println(movement);
		}
		
		if(this.debugging.contains("hashsize")){
			System.out.println("Hashtable contains " + this.hashSize + " boards.");
		}
		////////////////////////////////////////////////
		Scanner scan = new Scanner(movement);
		
		int blockRowStart = scan.nextInt();
		int blockColStart = scan.nextInt();
		scan.next(); //More block coordinates. Not needed.
		scan.next();
		String move = scan.next();
		String direction = scan.next();
		
		//Get block reference from tray.
		Block fromTray = tray[blockRowStart][blockColStart];
		moveBlock(fromTray, move, direction);
	}
	
	
	
	//Method that actually implements Moves using args passed in by
	//makeMove(). Method modifies tray, Block Object, and empty Object
	//to reflect changes caused by move. Empty is modified through
	//2 separate methods. 
	private void moveBlock(Block bl, String move, String direction){
		this.lstDirection = direction;
		///Get block reference
		Block b = tray[bl.getRowStart()][bl.getColStart()];
		///Modify empty 1st time
		this.RemovefromEmpty(b, move);
		
		
		///To move UP				
		if(move.equals("up")){
			///Change points in empty.
			int toAdd = b.getRowStart()-1;
			int toRemove = b.getRowEnd();
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				tray[toAdd][c] = b;		///Tray modified
				tray[toRemove][c] = null;
			}
		}
		
		///To move DOWN						
		if(move.equals("down")){
			///Change points in empty
			int toAdd = b.getRowEnd()+1;
			int toRemove = b.getRowStart();
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				tray[toAdd][c] = b;		///Tray modified
				tray[toRemove][c] = null;
			}
		}
		
		//To move LEFT						
		if(move.equals("left")){
			///Change points in empty
			int toAdd = b.getColStart()-1;
			int toRemove = b.getColEnd();
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				tray[r][toAdd] = b;		///Tray modified
				tray[r][toRemove] = null;
			}
		}
		
		//To move RIGHT					
		if(move.equals("right")){
			///Change points in empty.
			int toAdd = b.getColEnd() + 1;
			int toRemove = b.getColStart();
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				tray[r][toAdd] = b;		///Tray modified
				tray[r][toRemove] = null;
			}
		}
		
		///Change Block object instance variables to reflect move.
		b.move(move);
		
		if(direction.equals("forward")){
			String toSave = this.SaveMove(b, getOpposite(move), "reverse");
			this.Moves.push(toSave);
		}
		
		///Modify empty 2nd time.
		this.AddtoEmpty(b, move);
		
	}
	
	
	//First Empty modifier method. This is called before 
	//tray state is changed, as it needs the pre-move tray configuration
	//to work properly. Based on move direction & block position,
	//it changes empty to reflect the move made (partially).
	private void RemovefromEmpty(Block b, String move) {
		
		///If move is up.						
		if(move.equals("up")){
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				if(b.getRowEnd()+1 <= rows && tray[b.getRowEnd()+1][c] == null){
					this.empty.removeBlock(b);
				}
				this.empty.removePoint(new Point(b.getRowStart() - 1, c));
			}
			if(b.getColEnd()+1 <= cols && tray[b.getRowEnd()][b.getColEnd()+1]== null){
				this.empty.removeBlock(b);
			}
			if(b.getColStart() - 1 >= 0 && tray[b.getRowEnd()][b.getColStart()-1]==null){
				this.empty.removeBlock(b);
			}
		}
		
		//If move is down.						
		if(move.equals("down")){
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				if(b.getRowStart()-1 >= 0 && tray[b.getRowStart()-1][c] == null){
					this.empty.removeBlock(b);
				}
				this.empty.removePoint(new Point(b.getRowEnd()+1, c));
			}
			if(b.getColStart()-1 >= 0 && tray[b.getRowStart()][b.getColStart()-1] == null){
				this.empty.removeBlock(b);
			}
			if(b.getColEnd()+1 <= cols && tray[b.getRowStart()][b.getColEnd()+1]==null){
				this.empty.removeBlock(b);
			}
		}
		
		//If move is left.							
		if(move.equals("left")){
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				if(b.getColEnd()+1 <= cols && tray[r][b.getColEnd()+1]==null){
					this.empty.removeBlock(b);
				}
				this.empty.removePoint(new Point(r, b.getColStart()-1));
			}
			if(b.getRowStart() - 1 >= 0 && tray[b.getRowStart()-1][b.getColEnd()] == null){
				this.empty.removeBlock(b);
			}
			if(b.getRowEnd()+1 <= rows && tray[b.getRowEnd()+1][b.getColEnd()]== null){
				this.empty.removeBlock(b);
			}
		}
		
		//If move is right.							
		if(move.equals("right")){
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				if(b.getColStart()-1 >= 0 && tray[r][b.getColStart() - 1]==null){
					this.empty.removeBlock(b);
				}
				this.empty.removePoint(new Point(r, b.getColEnd()+1));
			}
			if(b.getRowStart()-1 >= 0 && tray[b.getRowStart()-1][b.getColStart()] == null){
				this.empty.removeBlock(b);
			}
			if(b.getRowEnd()+1 <= rows && tray[b.getRowEnd()+1][b.getColStart()]== null){
				this.empty.removeBlock(b);
			}
		}
	}
	
	//Second Empty modifier method. Called after tray and block objects are changed to reflect move
	//as it needs post-move state to work correctly. 
	private void AddtoEmpty(Block b, String move) {
		
		///If move is up.						
		if(move.equals("up")){
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				this.empty.addPoint(new Point(b.getRowEnd()+1, c));
				if(b.getRowStart()-1 >= 0 && tray[b.getRowStart()-1][c] == null){
					this.empty.addBlock(b);
				}
			}
			if(b.getColStart()-1>=0 && tray[b.getRowStart()][b.getColStart()-1]== null){
				this.empty.addBlock(b);
			}
			if(b.getColEnd()+1 <= cols && tray[b.getRowStart()][b.getColEnd()+1]==null){
				this.empty.addBlock(b);
			}
		}
		
		//If move is down.						
		if(move.equals("down")){
			for(int c = b.getColStart(); c <= b.getColEnd(); c++){
				this.empty.addPoint(new Point(b.getRowStart()-1, c));
				if(b.getRowEnd()+1 <= rows && tray[b.getRowEnd()+1][c] == null){
					this.empty.addBlock(b);
				}
			}
			if(b.getColStart()-1 >=0 && tray[b.getRowEnd()][b.getColStart()-1]== null){
				this.empty.addBlock(b);
			}
			if(b.getColEnd()+1 <= cols && tray[b.getRowEnd()][b.getColEnd()+1]==null){
				this.empty.addBlock(b);
			}
		}
		
		//If move is left.						
		if(move.equals("left")){
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				this.empty.addPoint(new Point(r, b.getColEnd()+1));
				if(b.getColStart()-1 >= 0 && tray[r][b.getColStart()-1] == null){
					this.empty.addBlock(b);
				}
			}
			if(b.getRowStart()-1 >= 0 && tray[b.getRowStart()-1][b.getColStart()]==null){
				this.empty.addBlock(b);
			}
			if(b.getRowEnd()+1<= rows && tray[b.getRowEnd()+1][b.getColStart()]==null){
				this.empty.addBlock(b);
			}
		}
		
		//If move is right.							
		if(move.equals("right")){
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				this.empty.addPoint(new Point(r, b.getColStart()-1));
				if(b.getColEnd()+1<= cols && tray[r][b.getColEnd()+1] == null){
					this.empty.addBlock(b);
				}
			}
			if(b.getRowStart()-1 >= 0 && tray[b.getRowStart()-1][b.getColEnd()]==null){
				this.empty.addBlock(b);
			}
			if(b.getRowEnd()+1<= rows && tray[b.getRowEnd()+1][b.getColEnd()]==null){
				this.empty.addBlock(b);
			}
		}
		
	}
	
	
	//Method takes in block and String of move, and checks whether that move is possible for 
	//the block by iterating through points which the block will move into. It ascertains that all
	//these points are null in the tray. Returns either true or false. 
	private boolean isPossible(Block bl, String move){
		Block b = tray[bl.getRowStart()][bl.getColStart()]; //Get Block reference from tray.

		if(move.equals("up")){ ///1. Check that moving up is possible.
			int r = b.getRowStart() - 1;
			if(r < 0){
				return false;  ///Cannot move up from top of board.
			}
			for(int c = b.getColStart(); c <= b.getColEnd(); c ++){
				if(tray[r][c] != null){
					return false;
				}
			}
			return true;
		}else if(move.equals("down")){ //2.Check that moving down is possible.
			int r = b.getRowEnd() + 1;
			if(r > rows){
				return false;
			}
			for(int c = b.getColStart(); c <= b.getColEnd(); c ++){  ///can't move down from bottom of board.
				if(tray[r][c] != null){
					return false;
				}
			}
			return true;
		}else if(move.equals("left")){ //3.Check that moving left is possible.
			int c = b.getColStart() - 1;
			if(c < 0){
				return false;		///can't move left from left-most point
			}
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				if(tray[r][c] != null){
					return false;
				}
			}
			return true;
		}else { ///4.Check that moving right is possible.
			int c = b.getColEnd() + 1;  
			if(c > cols){
				return false;   ///Can't move right from right-most point.
			}
			for(int r = b.getRowStart(); r <= b.getRowEnd(); r++){
				if(tray[r][c] != null){
					return false;
				}
			}
			return true;
		}
	}
	
	
	//A method for debugging. Can be used in debugging output. Prints out pretty crude
	//visual representation of the current tray. 0 in empty places & "B" at Block locations.
	public void printTray() {
		for(int r = 0; r <= rows; r++){
			for(int c = 0; c <= cols; c++){
				Block b = tray[r][c];
				if(b!= null){
					System.out.print("B");
				}else{
					System.out.print(0);
				}
			}
			System.out.println("");
		}
	}
	
	
	
	//toString() method of Board. The String representation of a Board is 
	//a combination of the toStrings() of all the Blocks on the Board,
	//with each Block's toString() separated by a "/". The String is found
	//by iterating through blockList.
	public String toString() {
		String rtn = "";
		for(int i = 0; i < blockList.size(); i++){
			Block curr = blockList.get(i);
			rtn = rtn + curr.toString() + "/";
		}
		return rtn;
	}
	
	
	//Returns the String representation of a move. This representation
	//is the form in which moves are stored on the Moves stack. A move is 
	//represented by the toString() of the target Block, the String of the
	//actual move ("up", "down", etc.), and the directionality of the move 
	//("forward" or "reverse"). 
	private String SaveMove(Block b, String move, String direction) {
		String block = b.toString();
		return block + " " + move + " " + direction;
	}
	
	
	//Checks whether this board matches goal board. 
	//This is done by comparing the toStrings() of this
	//Board with the goal String.
	private boolean isSame(String goal){
		String strThis = this.toString();
		String[] str = goal.split("/");
		for(int i = 0; i < str.length; i++){
			if (!strThis.contains(str[i])){
				return false;
			}
		}
		return true;
	}
	
	
	//A dummy method used for code readability.
	//Simply returns the result of a call to isSame.
	private boolean isComplete() {
		return this.isSame(this.goal);
	}
	
	//Returns a move that is the opposite of the input move. That is, moving a 
	//Block according to the argument move & then according to the returned 
	//move will return the Block to its original position.
	private String getOpposite(String move){
		if(move.equals("up")){
			return "down";
		}else if(move.equals("down")){
			return "up";
		}else if(move.equals("left")){
			return "right";
		}else {
			return "left";
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	//Method used to print the solution of a puzzle once the goal configuration is reached. Method	  /
	//iterates through the Moves stack, accumulating all moves marked as "reverse" (as these are the  /
	//opposites of the moves made to reach the current configuration). The opposite of these moves    /
	//are then printed.																				  /
	///////////////////////////////////////////////////////////////////////////////////////////////////
	public void printSolution(){
		ArrayList<String> rev = new ArrayList<String>();
		while(!Moves.empty()){
			String savedMove = Moves.pop();
			Scanner scan = new Scanner(savedMove);
			int posF1 = scan.nextInt();
			int posF2 = scan.nextInt();
			scan.next(); //No need for next two numbers.
			scan.next();
			String move = scan.next();
			String direction = scan.next();
			if(direction.equals("reverse")){
				if(move.equals("up")){
					int posI1 = posF1 - 1;
					int posI2 = posF2;
					rev.add(posI1 + " " + posI2 + " " + posF1 + " " + posF2);
				}else if (move.equals("down")){
					int posI1 = posF1 + 1;
					int posI2 = posF2;
					rev.add(posI1 + " " + posI2 + " " + posF1 + " " + posF2);
				}else if(move.equals("left")){
					int posI1 = posF1;
					int posI2 = posF2 - 1;
					rev.add(posI1 + " " + posI2 + " " + posF1 + " " + posF2);;
				}else{
					int posI1 = posF1;
					int posI2 = posF2 + 1;
					rev.add(posI1 + " " + posI2 + " " + posF1 + " " + posF2);
				}
			}
		}
		for(int i = rev.size()-1; i >= 0; i--){
			System.out.println(rev.get(i));
		}
	}
	
	
////////////////////////////////////////////////////////////////////
//Internal Class Empty. An Empty object stores the state of the    /
//empty portion of a Board, including a list of all Points in the  /
//Board which are empty, and all Blocks adjacent to empty points.  /
////////////////////////////////////////////////////////////////////
public class Empty {
	//A list of all empty Points in the Board.
	private ArrayList<Point> isEmpty;
	//A list of all Blocks adjacent to Empty points. Implementation
	//of Empty means that this list may contain repeats.
	private ArrayList<Block> nextToEmpty; 
			
	public Empty(){
		isEmpty = new ArrayList<Point>();
		nextToEmpty = new ArrayList<Block>();
		}
	
	//Initializes starting state of an empty object.
	//Used after all blocks are added to board. Iterates through all positions
	//in the tray and calls addPoint(Point p) on every position that holds null.
	public void updateisEmpty() {
		for(int c = 0; c <= cols; c++){
			for(int r = 0; r<=rows; r++){
				Block curr = tray[r][c];
				if(curr == null){
					Point p = new Point(r, c);
					addPoint(p);
				}
			}
		}
	}
	
	//Removes a Point from the Empty object. This means Point is removed from isEmpty list
	//and one occurrence of Blocks bordering this point are removed from the nextToEmpty list.
	private void removePoint(Point p){
		isEmpty.remove(p);
		ArrayList<Point> around = p.neighbours(rows, cols);
		for(int i =0; i<around.size(); i++){
			Point curr = around.get(i);
			if(tray[curr.getRow()][curr.getCol()] != null){
				this.nextToEmpty.remove(tray[curr.getRow()][curr.getCol()]);
			}
		}
	}
	
	//Adds a Point to the Empty object. This means that the Point is added to isEmpty, and
	//for every Block bordering the Point, a reference is added in nextToEmpty.
	private void addPoint(Point p){
		isEmpty.add(p);
		ArrayList<Point> around = p.neighbours(rows, cols);
		for(int i =0; i<around.size(); i++){
			Point curr = around.get(i);
			if(tray[curr.getRow()][curr.getCol()] != null){
				this.nextToEmpty.add(tray[curr.getRow()][curr.getCol()]);
			}
		}
	}
	
	//Removes one occurrence of the Block b from the nextToEmpty list. 
	//Method used when a Point does not change to nonEmpty (from empty), but 
	//when a Block object no longer borders that Point.
	private void removeBlock(Block b){
		nextToEmpty.remove(b);
	}
	
	//Adds one occurrence of the Block b to the nextToEmpty list.
	//Method used when a Point does not change to empty (from non-empty),
	//but when a Block moves to border an existing empty point.
	private void addBlock(Block b){
		nextToEmpty.add(b);
	}
	
	
	
	//A print() method for the Empty class. Provides information about the state 
	//of the Empty object. Used for debugging.
	public void print(){
		for(int i = 0; i < nextToEmpty.size(); i++){
			System.out.print(nextToEmpty.get(i) + "/");
		}
		System.out.println(" ");
	}
	
}
///////////////////////////////////////////////////////////////////////////////////////////////
////End of internal class Empty.															 //
///////////////////////////////////////////////////////////////////////////////////////////////



}

	 
	
	

