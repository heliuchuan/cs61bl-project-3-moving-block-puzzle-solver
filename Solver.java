import java.util.ArrayList;
import java.util.Scanner;


public class Solver {	
	
	///set up the root board from the input files
	public static Board setBoard(InputSource input, String type, int rows, int cols){
		Board result = new Board(rows, cols);
		String line = input.readLine();
		while(line != null){
			result.addBlock(new Block(line));
			line = input.readLine();
		}
		
		if(type.equals("start")){
			//After adding all Blocks, the empty of the Board must be updated. This involves iteration
			//through the tray. Therefore, to save time, is not performed on the goal board.
			result.EmptyUpdate();	
		}
		return result;
	}
	
	//Called if -ooptions entered as debugging output. Prints out information on 
	//all available debugging options.
	public static void printOptions() {
		
	}
	
	
	public static void main (String [ ] args) throws Exception {
		
		if(args.length == 1){
			if(!args[0].equals("-ooptions")){
				throw new IllegalArgumentException("Bad arguments");
			}else{
				printOptions();
			}
			
		}else{
			ArrayList<String> debugOptions = new ArrayList<String>();
			InputSource initialLines = new InputSource(args[args.length-2]);
			InputSource goalLines = new InputSource(args[args.length-1]);
			
			if(args.length > 2) {
				
				for(int i = 0; i < args.length-2; i++){
					String debugStr = args[i];
					if(!debugStr.substring(0, 2).equals("-o")){
						throw new IllegalArgumentException("Invalid debug options.");
					}
					debugOptions.add(debugStr);
				}
			}
			
			///Get board dimensions
			String dimensions = initialLines.readLine();
			Scanner d = new Scanner(dimensions);
			int rows = d.nextInt();
			int cols = d.nextInt();
			
			Board start = setBoard(initialLines, "start", rows, cols); 	//empty update included
			Board goal = setBoard(goalLines, "goal", rows, cols);
			
			start.setGoal(goal);
			start.setDebuggingOutput(debugOptions);
	
			String result = start.Solve();
			
			if (result.equals("No solution.")){
				System.exit(0);
			}else if(result.equals("solved")){
				start.printSolution(); 
			}
		}
	}
	

}
