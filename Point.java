import java.util.*;
/////////////////////////////////////////////////////////
//A Point object is a specific point on a Board. It is  /
//defined by two variables: row and column.             /
/////////////////////////////////////////////////////////
public class Point {
	private int row;
	private int col;
	
	public Point(int row, int col){
		this.row = row;
		this.col = col;
	}
	
	//Returns an ArrayList containing all Point objects that are
	//neighbors of this Point. The arguments specify the boundaries
	//of the Board which this Point is part of. 
	public ArrayList<Point> neighbours(int maxRows, int maxCols) {
		ArrayList<Point> nextTo = new ArrayList<Point>();
		if(col+1 <=maxCols){
			nextTo.add(new Point(row, col+1));
		}
		if(col-1 >=0){
			nextTo.add(new Point(row, col-1));
		}
		if(row+1 <=maxRows){
			nextTo.add(new Point(row+1, col));
		}
		if(row-1 >=0){
			nextTo.add(new Point(row-1, col));
		}
		
		return nextTo;
	}
	
	//Methods for encapsulation. Return row and column of Point object.
	public int getRow() {
		return this.row;
	}
	
	public int getCol() {
		return this.col;
	}
	//////////////////////////////////////////////////////////////////////
	
	//Returns a String representation of this Point. 
	public String toString(){
		return this.row + "  " + this.col;
	}
	
	//Determines if this Point is equal to another by comparing their
	//row and column instance variables. Returns true or false.
	public boolean equals(Object o){
		if((this.col==((Point)o).col)&&(this.row==((Point)o).row)){
			return true;
		}
		return false;
	}
}
